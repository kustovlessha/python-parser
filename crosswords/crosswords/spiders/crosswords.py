import scrapy
import mysql.connector
import redis

class CrosswordsSpider(scrapy.Spider):
    name = "crosswords"
    baseUrl = "https://www.kreuzwort-raetsel.net/"
    r = redis.Redis(host='redis', port=6379)
    queueName = "toParse"

    start_urls = [
        'https://www.kreuzwort-raetsel.net/uebersicht-zeichen.html',
    ]

    CUSTOM_PROXY = "http://202.165.66.21:3128"

    def parse(self, response, **kwargs):
        if self.r.scard(self.queueName) < 1:
            if response.request.url == 'https://www.kreuzwort-raetsel.net/uebersicht.html':
                for href in response.css('ul.dnrg li a::attr("href")').extract():
                    url = self.baseUrl + href
                    worker = "letters_page"
                    queue_row = worker + "|" + url
                    self.r.sadd(self.queueName, queue_row)

            if response.request.url == 'https://www.kreuzwort-raetsel.net/uebersicht-zeichen.html':
                for href in response.css('ul.dnrg li a::attr("href")').extract():
                    url = self.baseUrl + href
                    worker = "numbers_page"
                    queue_row = worker + "|" + url
                    self.r.sadd(self.queueName, queue_row)

        while True:
            if self.r.scard(self.queueName) > 0:
                row = str(self.r.spop(self.queueName).decode("utf-8"))
                for_parse = row.split("|")
                parse_url = for_parse[1]
                parse_worker = for_parse[0]

                if parse_worker == "letters_page":
                    method = self.letters_page_parse
                elif parse_worker == "single_number_page":
                    method = self.single_number_page
                elif parse_worker == "single_letter_page":
                    method = self.single_letter_parse
                elif parse_worker == "parse_question":
                    method = self.parse_question
                elif parse_worker == "parse_answer":
                    method = self.parse_answer
                elif parse_worker == "numbers_page":
                    method = self.numbers_page_parse

                request = scrapy.Request(parse_url, callback=method)
                request.meta['proxy'] = self.CUSTOM_PROXY
                yield request
            yield True

    # Парсинг страницы с алфавитом
    def letters_page_parse(self, response, **kwargs):
        for href in response.css('ul.dnrg li a::attr("href")').extract():
            url = self.baseUrl + href
            worker = "single_letter_page"
            queue_row = worker + "|" + url
            self.r.sadd(self.queueName, queue_row)

    # Парсинг страницы с номером
    def numbers_page_parse(self, response, **kwargs):
        for href in response.css('ul.dnrg li a::attr("href")').extract():
            url = self.baseUrl + href
            worker = "single_number_page"
            queue_row = worker + "|" + url
            self.r.sadd(self.queueName, queue_row)

    # Парсинг страницы со списком разделов по букве
    def single_letter_parse(self, response, **kwargs):
        for line in response.css('.ContentElement table tbody tr'):
            question_url = line.css('.Question a::attr("href")').extract_first()
            url = self.baseUrl + question_url
            worker = "parse_question"
            queue_row = worker + "|" + url
            self.r.sadd(self.queueName, queue_row)

    # Парсинг страницы со списком разделов по букве
    def single_number_page(self, response, **kwargs):
        for line in response.css('.ContentElement table tbody tr td.AnswerShort'):
            answer_url = line.css('a::attr("href")').extract_first()
            url = self.baseUrl + answer_url
            worker = "parse_answer"
            queue_row = worker + "|" + url
            self.r.sadd(self.queueName, queue_row)

            print("=========== single_number_page")
            print(queue_row)

    # Парсинг страницы с ответом
    def parse_answer(self, response, **kwargs):
        answer = response.css('.Text h1 span#HeaderString::text').extract_first().strip().lower()
        answer_id = self.check_and_add_answer(answer)

        for question_row in response.css('.ContentElement table tbody tr'):
            if question_row is not None:
                question = question_row.css('td.QuestionShort a::text').extract_first().strip().lower()
                question_url = question_row.css('td.QuestionShort a::attr("href")').extract_first().strip()

                question_id = self.check_and_add_question(question, question_url)
                self.answer_question_relation(answer_id, question_id)

                print("============ \n question_id")
                print(question_id)

    # Парсинг страницы с вопросом
    def parse_question(self, response, **kwargs):
        question = response.css('.Text h1 span#HeaderString::text').extract_first().strip().lower()
        question_id = self.check_and_add_question(question)

        for answer_row in response.css('.ContentElement table tbody tr'):
            if answer_row is not None:
                answer = answer_row.css('td.Answer a::text').extract_first().strip().lower()
                answer_url = answer_row.css('td.Answer a::attr("href")').extract_first().strip()
                answer_length = answer_row.css('td.Length::text').extract_first().strip()

                answer_id = self.check_and_add_answer(answer, answer_length, answer_url)
                self.answer_question_relation(answer_id, question_id)
            if self.r.scard(self.queueName) > 0:
                self.parse(response)

    # answer_url помогает определить нужно ли парсить страницу ответа
    def check_and_add_answer(self, answer, answer_length=None, answer_url=None):
        connection = mysql.connector.connect(
            user='myuser', password="mypassword", host="db", port="3306", database='crossword'
        )
        db = connection.cursor()

        db.execute('Select * FROM answers WHERE answer = %s LIMIT 1', [answer])
        db_answer = db.fetchone()

        if db_answer is None:
            db.execute('Insert into answers (`answer`, `length`) values (%s, %s)', [answer, answer_length])
            connection.commit()
            answer_id = db.lastrowid
            if answer_url is not None:
                url = self.baseUrl + answer_url
                worker = "parse_answer"
                queue_row = worker + "|" + url
                self.r.sadd(self.queueName, queue_row)
        else:
            answer_id = db_answer[0]

        db.close()
        return answer_id

    def check_and_add_question(self, question, question_url=None):
        connection = mysql.connector.connect(
            user='myuser', password="mypassword", host="db", port="3306", database='crossword'
        )
        db = connection.cursor()

        db.execute('Select * FROM questions WHERE question = %s LIMIT 1', [question])
        db_question = db.fetchone()

        if db_question is None:
            db.execute('Insert into questions (`question`) values (%s)', [question])
            connection.commit()
            question_id = db.lastrowid
            if question_url is not None:
                url = self.baseUrl + question_url
                worker = "parse_question"
                queue_row = worker + "|" + url
                self.r.sadd(self.queueName, queue_row)
        else:
            question_id = db_question[0]

        db.close()
        return question_id

    def answer_question_relation(self, answer_id, question_id):
        connection = mysql.connector.connect(
            user='myuser', password="mypassword", host="db", port="3306", database='crossword'
        )
        db = connection.cursor()

        db.execute('Select * FROM answer_to_question WHERE answer_id = %s AND question_id = %s', [answer_id, question_id])
        db_relation = db.fetchone()

        if db_relation is None:
            db.execute('Insert into answer_to_question (`answer_id`, `question_id`) values (%s, %s)', [answer_id, question_id])
            connection.commit()