use crossword;

--
-- Структура таблицы `answers`
--

CREATE TABLE `answers` (
    `id` int UNSIGNED NOT NULL,
    `answer` varchar(255) NOT NULL,
    `length` tinyint UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Структура таблицы `questions`
--

CREATE TABLE `questions` (
    `id` int UNSIGNED NOT NULL,
    `question` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Структура таблицы `answer_to_question`
--

CREATE TABLE `answer_to_question` (
    `id` int UNSIGNED NOT NULL,
    `answer_id` int UNSIGNED NOT NULL,
    `question_id` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `answers`
--
ALTER TABLE `answers`
    ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `answer` (`answer`);

--
-- Индексы таблицы `answer_to_question`
--
ALTER TABLE `answer_to_question`
    ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `answer_id` (`answer_id`,`question_id`);

--
-- Индексы таблицы `questions`
--
ALTER TABLE `questions`
    ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `question` (`question`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `answers`
--
ALTER TABLE `answers`
    MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT для таблицы `answer_to_question`
--
ALTER TABLE `answer_to_question`
    MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT для таблицы `questions`
--
ALTER TABLE `questions`
    MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
COMMIT;